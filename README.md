General Instructions
================
--------------------------------

1. Clone this repo directly into your ~/Documents/StarCraft II/Maps folder. 
2. Open these maps via the StarCraft II Editor
3. Edit the scripts outside of the Editor itself using Atom or Sublime
	
    > Follow the Conventions listed below to implement your behaviors.
    > 
	> **Please do not alter the triggers emplaced in the maps themselves.**

Use of Readme
============
------------------------------

* Use Table of Contents to navigate through the sections
* Review the edit log for recent changes
	* Remember to add you signature and date after you review recent changes!
* When you make a change, create an entry at the top of the Edit Log with the date of the change and you signature.
* Questions regarding the content of the Readme should be handled via the chat group, not through direct edits to the Readme.
	* Decisions of the questions should be added to the Readme and noted with an Edit Log

Links
====
----------

* [Weekly Task Sheet](https://docs.google.com/spreadsheets/d/1PPKj3eo0V72VUrtrKSG7Avr1KNN4o49CvZyNPyrXn3U/edit?pref=2&pli=1#gid=2065724013)
* [Google Drive](https://drive.google.com/drive/u/1/folders/0B9QSOiMEhNryclBQUTJrQ2UxN2s)
* [Project Website](http://www-scf.usc.edu/~siddhigu/524progress.html)
* [SC2Mapster Wiki](http://www.sc2mapster.com/wiki/galaxy/script/)
* [List of Native Galaxy Script Functions](https://github.com/grum/sc2-dev/blob/master/Mods/Core.SC2Mod/Base.SC2Data/TriggerLibs/natives.galaxy)

Table of Contents
==============
----------------------------------
[TOC]

AnyBot Conventions
================
----------------------------------------

## Trigger Editor

> The trigger editor is used only to create win/loss conditions and trigger our main script.
> 
> **Behaviors should be added via GalaxyScript files rather than through the editor.**

The trigger editor is used to import our galaxy scripts into the main script itself. Apart from this, the trigger editor is only used to create win/loss conditions for the game, as well as remove the fog of war and initialize our main script through the use of `mapInitiated()`. All other trigger handling is done through the scripts themselves so that they can maintain conventions outlined below and effectively interface with the global data stored in the Blackboard.

> The trigger editor, however, is the best resource for exploring native functions.
> 
> In addition, use this link to find even more native functions: [List of Native Functions](https://github.com/grum/sc2-dev/blob/master/Mods/Core.SC2Mod/Base.SC2Data/TriggerLibs/natives.galaxy "List of Native Functions")
> 
> **Steps to find native functions:**
> 
> 1. Create a new trigger via the Gear icon.
> 2. Add a new item for whatever function you are looking for:
> a. If looking for a function to fire a trigger, add a new event.
> b. If looking for a function to cause an action, add a new action.
> c. If looking for a function to return a variable, add a new variable.
> d. If looking for a function to return a state, add a new condition.
> 3.  Ensure that the trigger editor is not showing Raw Data.
> a. _Icon that looks like a DNA strand is not highlighted._
> 4. Double click the item that you created to bring up the box of available functions.
> a. Browse this list until you find a function that fits your needs.
> b. Clicking on a function in the list will bring up more details in the text field at the bottom of the window.
> 5. Double click on the function that you have selected.
> 6. In the toolbar, open the "Data" menu and select "View Script".
> 7. In the script pop-up, scroll to find the implementation of the trigger that you created.
> a. If you are looking for an event, instead look for the init function rather than the imp function.
> 8. Here you will find the function that is being called to create your desired behavior. Copy and paste this into your user-defined trigger within your script.
> 9. Lastly, ensure that the parameters being passed into the function are correct for your event.
> a. _Often times, the function that this script creates will use EventUnit() or other standards. This may not be correct for your desired behavior._

## General Process

> **Modified Blackboard**
> 
> - Shared data stored in Blackboard.galaxy via globals
> - Triggers first post observed data into the Blackboard
> - Triggers then assign orders to the end of the current queue
> - **NO** Arbiter


The overall architecture is a modified version of the Blackboard problem solving strategy. In this strategy, each trigger that is called records the relevant information that it has gathered into the global Blackboard, which is available for use by all other triggers. This allows each trigger to work independently, while sharing the same shared data bank. However, unlike the traditional Blackboard strategy, there is no arbiter that choses what solution to go with at each given time. Rather, each trigger responds autonomously, and so it is up to the developers to ensure that these behaviors to not conflict. This is outlined further in the Conventions section of this Readme document. This is the largest downfall of this simplified implementation of the Blackboard strategy (used to attempt to not inhibit processing limitations of Galaxy Script) as relevance of each solution is not accounted for by default. This may require a change in the future.

## Blackboard

> **Script to house ALL shared data (i.e. globals).**
> 
> - Structs
> - Globals
> - **NO** Functions

Included in the Macro map is a file called `Blackboard.galaxy`. This file is intended solely to house all shared data for the AnyBot AI. This houses all structs and global variables used in all scripts. The intention of this consolidation is to enforce the Blackboard strategy outlined above, and to ensure that all behaviors work off of the same shared data rather than working with multiple copies of the same data. In addition, this will ensure consistency between data types used throughout the various scripts.

### Blackboard Structs

> Anthology of all structs used on the Blackboard.
> 
> Format:
> 
> - Struct Name - _Purpose_
>     - Member Variable - _Purpose_

* EnemyUnit - _Used to track all actual units owned by the enemy._
    * unit enemy - _The actual unit base type that we wish to track. Many attributes can be gathered from this variable using GalaxyScript's native library._
    * fixed assignedDamage - _The amount of damage assigned to this unit. This is used within the Focus Fire script in order to efficiently assign units to attack._
* EnemyStructure - _Used to track all structure units owned by the enemy._
    * unit enemy - _The actual unit base type that we wish to track. Many attributes can be gathered from this variable using GalaxyScript's native library._
    * fixed assignedDamage - _The amount of damage assigned to this unit. This is used within the Focus Fire script in order to efficiently assign units to attack._
* FriendlyUnit - _Used to track all actual units owned by AnyBot._
    * unit friendly - _The actual unit base type that we wish to track. Many attributes can be gathered from this variable using GalaxyScript's native library._
    * bool assigned - _A variable to check if this unit has already been assigned to attack. Used by the Focus Fire script._
    * string groupName - _The unit group that this unit has been assigned to. This can be used to find the actual group from the group list._
* FriendlyStructure - _Used to track all structures owned by AnyBot._
    * unit friendly - _The actual unit base type that we wish to track. Many attributes can be gathered from this variable using GalaxyScript's native library._
    * bool assigned - _A shell variable used to track if this structure has been assigned. Not currently used in any scripts._ - **Flagged for deletion for reason: Native Library already allows us to see queue for structures. No script needs additional control at the moment.**
* FriendlyUnitGroup - _Used to track all unit groups owned by AnyBot. These are where large group orders can be issued without iterating over all units._
    * string groupName - _The name used to identify this group. Segmented into *AttackWave#* and *SCVGroup#* in this current iteration._
    * unitgroup group - _The actual unitgroup base type that we wish to track. Many attributes can be gathered from this variable using GalaxyScript's native library._
    * int expectedSize - _The number of units that this group should expect to have if it is an attack wave. Currently used in Macro script to determine when to send the attack wave to attack the enemy base._
* Purchase - _Used to track all purchases that AnyBot should make. All attributes used in the Macro script._
    * string item - _The unit type (or other identifying factor) of the item that AnyBot wishes to purchase._
    * int mineralPrice - _The number of minerals that the item costs. Tracked here for simplicity of calls and potential stalling points by artificially inflating this value._
    * int vespenePrice - _The amount of vespene gas that the item costs. Tracked here for simplicity of calls and potential stalling points by artificially inflating this value._
    * int priority - _The urgency of this purchase. Currently not used in any script, but will later be used in the Macro script to determine when a large purchase can be passed up by a smaller purchase._
    * string structureToPurchaseFrom - _The unit type (or other identifying factor) of the unit that must actually make the purchase._

### Blackboard Globals

> Anthology of all globals used on the Blackboard.
> 
> Format:
> 
> - Section/Data Purpose
>     - Global Variable - _Purpose/Usage_

* Debug Variables
    * bool gDebugChatOn - _When true, debug messages will be displayed in the chat._
    * bool gMineralCountOn - _When both this and_ `gDebugChatOn` _are true, AnyBot's mineral count will be displayed every time a mineral or vespene gas is collected._
* Data About AnyBot
    * int gTeamNumber - _AnyBot's team number. By default this is 2 as the human player is player 1._
    * int gMyUnitCount - _The current number of actual units owned by AnyBot and alive. This is used in conjunction with_ `gMyUnitList` _as GalaxyScript does not have a native count for arrays._
    * FriendlyUnit[1000] gMyUnitList - _The list of all actual units owned by AnyBot and alive. Use_ `gMyUnitCount` _to get the actual number of units stored in this array._
    * string[24] gMyPossibleUnits - _A listing of all possible unit names for AnyBot's units. This is set up in the Main script and used in the Utils script to categorize units. **Note that this assumes that AnyBot's enemy is not Terran.**_
    * int gCurrentlyCreatingAttackGroup - _The number of the attack group we are currently creating. This is used in the Macro script as well as the Utils script. This is incremented only when the current attack wave has been completed._
    * int gCurrentAttackGroupExpectedSize - _The expected number of units to be built in the attack wave. This is used in the Setup scripts (currently MarineRush) and the Utils script._
    * int gCurrentlyCreatingSCVGroup - _The number of the SCV group we are currently creating. This is used in the Utils script._
    * FriendlyUnitGroup[20] gMyUnitGroupList - _The list of all unit groups owned by AnyBot. Use_ `gMyUnitGroupCount` _to get the actual number of groups stored in this array._
    * int gMyUnitGroupCount - _The current number of unit groups owned by AnyBot. This is used in conjunction with_ `gMyUnitGroupList`
    * int gMyStructureCount - _The current number of structure units owned by AnyBot and alive. This is used in conjunction with_ `gMyStructureList`
    * FriendlyStructure[1000] gMyStructureList - _The list of all structure units owned by AnyBot and alive. Use_ `gMyStructureCount` _to get the actual number of groups stored in this array._
    * string[30] gMyPossibleStructures - _A listing of all possible unit names for AnyBot's structures. This is set up in the Main script and used in the Utils script to categorize units. **Note that this assumes that AnyBot's enemy is not Terran.**_
    * int gPurchaseListCount - _The current number of purchases queued for AnyBot. This is used in conjunction with_ `gPurchaseList`
    * Purchase[1000] gPurchaseList - _The list of all purchases queued for AnyBot. Use_ `gPurchaseListCount` _to get the actual number of purchases stored in this array._
* Data About the Human Player
    * int gEnemyUnitCount - _The current number of actual units owned by the human player and alive. Used in conjunction with_ `gEnemyUnitList`
    * EnemyUnit[1000] gEnemyUnitList - _The list of all actual units owned by the human player and alive. Use_ `gEnemyUnitCount` _to get the actual number of enemy units stored in this array._
    * int gEnemiesInRangeCount - _The current number of enemy units in range of the active unit group. Used in Spread script. Used in conjunction with_ `gEnemiesInRange` - **Marked for potential deletion upon restructuring of Spread script.**
    * EnemyUnit[1000] gEnemiesInRange - _The list of enemy units in range of the active unit group. Used in Spread script. Use_ `gEnemiesInRangeCount` _to get the actual number of enemy units in range._ - **Marked for potential deletion upon restructuring of Spread script.**
    * int gEnemyStructureCount - _The current number of structure units owned by the human player and alive. Used in conjunction with_ `gEnemyStructureList`
    * EnemyStructure[1000] gEnemyStructureList - _The list of structure units owned by the human player and alive. Use_ `gEnemyStructureCount` _to get the actual number of structures stored in the array._
    * string[23] gEnemyPossibleStructures - _A listing of all possible structure names for the human player. Set up in the Main script and used in the Utils script to categorize units. **Note that this assumes that the human player is Zerg**_

## Utils

> **Script housing widely used utility functions.**
> 
> Use these whenever possible as opposed to implementing a new function/process.
> 
> All array insertion and deletion should be done via a Utils function.

Included in the Macro map is a file called `Utils.galaxy`. This file is used to compile common calls used within triggers or common trigger responses needed by several behaviors to build the shared memory in the Blackboard. Methods and triggers should only be placed in this file when they are useful to be called in multiple different types of behaviors or set up memory that is important for use in several behaviors. For set up functions or helper functions used in only one behavior, please keep those methods within that behavior's script rather than placing them in the Utils file.

### Utils Trigger List

* `UtilsOnUnitCreated(bool checkConds, bool runActions)` 
    * Called when any unit is created
    * First determines which team the unit is on
    * Next determines if the unit is a structure or an actual unit
    * Appends the appropriate struct to the appropriate unit list _(setting default values for member variables)_.
    * Increments the unit count for the appropriate list.
    * Notable Exceptions:
        * SCV's - Issued an order to begin mining the nearest mineral. Added to current SCV group
        * All other ally units - Added to the current attack wave unitgroup _(if unitgroup is not already created, creates one)_.
* `UtilsOnUnitDied(bool checkConds, bool runActions)`
    * Called when any unit dies
    * First determines which team the unit is on
    * Next determines if the unit is a structure or an actual unit
    * Shifts every unit at a higher index of the appropriate unit list down one space _(overwriting the index where the dying unit used to exist)_.
    * Decrements the unit count for the appropriate list.
    * Notable Exceptions:
        * N/A

### Utils Function List

> Format:
> 
> #### Section
> * `FunctionName(Parameters)`
>     * `Parameter Name` = Description
>     * "Description:" _Actual Description_
>     * "Returns:" Return Type
>         * Explanation of returned value
>     * Notes:
>         * Note

#### Determine Unit Ownership/Type

* `UtilsIsUnitMyUnit(unit u)` 
    * `Unit u` = The unit you wish to check 
    * Description: _This function will check if the requested unit is an actual unit (as opposed to a structure) on AnyBot's team. _
    * Returns: bool
	    * true = Unit u is on AnyBot's team AND is an actual unit (not a structure)
	    * false = Unit u is EITHER not on AnyBot's team OR a structure unit
* `UtilsIsUnitMyStructure(unit u)` 
	* `Unit u` = The unit you wish to check 
	* Description: _This function will check if the requested unit is a structure on AnyBot's team._ 
	* Returns: bool
		* true = Unit u is on AnyBot's team AND is a structure unit
		* false = Unit u is EITHER not on AnyBot's team OR an actual unit]
* `UtilsIsUnitEnemyStructure(unit u)` 
	- `Unit u` = The unit you wish to check 
	- Description: _This function will check if the requested unit is a structure that is not on AnyBot's team._
	- Returns: bool
		* true = Unit u is not on AnyBot's team AND is a structure unit
		* false = Unit u is EITHER on AnyBot's team OR an actual unit

> _Note: Checking for an enemy unit is omitted from these utils as it is deemed unnecessary when the other checks are used, as they are often used together. When using them in sequence and all return false, then the only logical solution is that the unit is an enemy actual unit._

#### Add/Remove Units from Tracking

* `UtilsDeleteMyUnit(unit u)`
	* `Unit u` = The unit you wish to delete
	* Description: _This function will attempt to delete the requested unit from AnyBot's actual unit list._ 
	* Returns: bool
		* true = Unit u was found in AnyBot's actual unit list AND was deleted successfully
		* false = Unit u was NOT found in AnyBot's actual unit list
	* Notes: 
		* This is intended to be the sole point of contact for removing units from AnyBot's actual unit list. 
		* This does not ensure that the unit is an actual unit owned by AnyBot. It is more efficient to use `UtilsIsUnitMyUnit(unit u)` prior to this method as the searched list in that method is much smaller.
* `UtilsDeleteMyStructure(unit u)` 
	* `Unit u` = The unit you wish to delete
	* Description: _This function will attempt to delete the requested unit from AnyBot's structure unit list._
	* Returns: bool
		* true = Unit u was found in AnyBot's structure unit list AND was deleted successfully 
		* false = Unit u was NOT found in AnyBot's structure unit list
	* Notes:
		* This is intended to be the sole point of contact for removing units from AnyBot's structure unit list. 
		* This does not ensure that the unit is a structure unit owned by AnyBot. It is more efficient to use `UtilsIsUnitMyStructure(unit u)` prior to this method as the searched list in that method is much smaller.
* `UtilsDeleteEnemyUnit(unit u)` 
	* `Unit u` = The unit you wish to delete
	* Description: _This function will attempt to delete the requested unit from the enemy player's actual unit list._
	* Returns: bool
		* true = Unit u was found in the enemy's actual unit list AND was deleted successfully
		* false = Unit u was NOT found in the enemy's actual unit list
	* Notes:
		* This is intended to be the sole point of contact for removing units from the enemy's actual unit list. 
		* This does not ensure that the unit is an actual unit owned by the enemy. It is more efficient to chain the other checking Utils functions prior to this method as the searched list in those methods are much smaller.
* `UtilsDeleteEnemyStructure(unit u)` 
	* `Unit u` = The unit you wish to delete 
	* Description: _This function will attempt to delete the requested unit from the enemy's structure unit list._ 
	* Returns: bool
		* true = Unit u was found in the enemy's structure list AND was deleted successfully
		* false = Unit u was NOT found in the enemy's structure list
	* Notes: 
		* This is intended to be the sole point of contact for removing units from the enemy's structure unit list. 
		* This does not ensure that the unit is a structure unit owned by the enemy. It is more efficient to use `UtilsIsUnitEnemyStructure(unit u)` prior to this method as the searched list in that method is much smaller.

#### Add/Delete Purchase Requests

* `UtilsAddPurchaseRequest(string item, int mineralPrice, int vespenePrice, int priority, string structure)` 
	* `String item` = The type of item you wish to purchase. 
	* `Int mineralPrice` = The number of minerals required to purchase this item. 
	* `Int vespenePrice` = The amount of vespene gas required to purchase this item. 
	* `Int priority` = The urgency of this purchase **(1-100 ONLY)**. 
	* `String structure` = The type of structure (or unit) that can make this purchase. 
	* Description: _This function will add a purchase request to the list of AnyBot's requested purchases. This is used to create build orders._
	* Returns: N/A
* `UtilsMadePurchase(int index)` 
	* `Int index` = The index of the purchase request that has been fulfilled. 
	* Description: _This function will delete the specified purchase request since it has been fulfilled. It will shift all later purchase requests down to maintain list integrity._

## Conventions

Below is a listing of conventions used by all scripts in order to ensure that they interact properly.

* General Methods
    * Method names should always begin with the name of the script first. _For example, the utils On Unit Created trigger has the method name_ `UtilsOnUnitCreated` _rather than simply_ `OnUnitCreated`.
    * Structs should never be returned from a method. _This is due to GalaxyScript having no copy constructor, so I don't believe this is even syntactically valid._
* Triggers
    * All triggers should be registered in `Main.galaxy` in the method `RegisterTriggers()`.
    * Trigger callbacks should all start with the name of the script first. _For example, the utils On Unit Created trigger has the method name_ `UtilsOnUnitCreated` _rather than simply_ `OnUnitCreated`.
* Globals
    * All globals should be declared in `Blackboard.galaxy`.
    * Globals should begin with the lower-case letter **g**. _For example, the variable to turn on or off debug chat is_ `gDebugChatOn` _rather than_ `debugChatOn`.
    * Globals should be added only if existing global variables are not sufficient. This will help prevent memory clutter.
    * If data needs to be tracked about individual units or entities, the data should be added to the struct and all shifting functions in Utils should be updated prior to pushing any code.
* Utils
    * Utils are for often repeated general tasks, and these tasks only. Do not put script-specific tasks in this file.
    * Any shifting of places inside of an array should be done within Utils so that updates to those structs can be handled simply. _(For example,_ `UtilsDeleteMyUnit(unit u)`_)_
    * On the other hand, if a task that you use in your script will likely be repeated elsewhere _(i.e. identifying a unit's ownership and type)_, then add this method to the Utils script.
    * All methods in the Utils script should begin with the word *Utils* as outlined in the trigger callbacks and general methods section.
* Unit Orders
    * Many scripts order units based on many different triggers. Because of this, the potential for overwriting tasks assigned in other triggers can cause issues with AnyBot's overall behavior.
    * Whenever possible, use `c_orderQueueAddToEnd`. This will allow the unit to execute any other tasks that have been assigned to it before executing the new task. _Note that when issuing orders to SCVs that are currently harvesting minerals,_ `c_orderQueueAddToEnd` _will not be executed until the minerals have been exhausted. In this special case, use_ `c_orderQueueAddToFront` _by default instead, even though this may cause issues with other behaviors._
    * If the task must be completed right away, use `c_orderQueueAddToFront`. This will cause the unit to immediately execute your task, and then return to whatever it was doing before.
    * Only in extreme circumstances should `c_orderQueueReplace` be used. When this is used, comment this usage with the justification for using Replace over Add To Front.


Edit Log
=======
-----------------
## Complete Creation
> Date: 9/2/16
> 
> **Sections:**
> 
> * General Instructions
> * Use of Readme
> * Links
> * AnyBot Conventions
>     * Trigger Editor
>     * General Process
>     * Blackboard
>         * Blackboard Structs
>         * Blackboard Globals
>     * Utils
>         * Utils Trigger List
>         * Utils Function List
>             * Section
>             * Determine Unit Ownership/Type
>             * Add/Remove Units from Tracking
>             * Add/Delete Purchase Requests
>     * Conventions
> 
> **Read Signature**
> 
> * Patrick Bradshaw: 9/2/16